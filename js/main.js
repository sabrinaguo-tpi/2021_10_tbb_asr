// =================== function  =====================
//關閉welcome畫面
$(".welcome-tip-cover").on("click", function () {
  $(this).fadeOut();
  $(".mic-tip-txt").fadeIn();
});
//toggle sound off/open
var soundOpen = false; //開啟聲音或關閉
$(".soundControl").on("click", function () {
  soundOpen = !soundOpen;
  if (soundOpen) {
    $(this).find("img").attr("src", "../images/ic-sound-open.svg");
  } else {
    $(this).find("img").attr("src", "../images/ic-sound-off.svg");
  }
});

//可惡100vh
var vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty("--vh", `${vh}px`);

// showMoreBtn
function showMoreBtn() {
  $(".md-btn").show();
  var scrollElement = document.querySelectorAll(".inner-scroll");
  var nScrollHight = 0; //滾動距離總長(注意不是滾動條的長度)
  var nScrollTop = 0; //滾動到的當前位置
  var nDivHight = 0; //元素高度
  scrollElement.forEach(function (item) {
    item.addEventListener("scroll", function () {
      nScrollHight = item.scrollHeight;
      nScrollTop = item.scrollTop;
      nDivHight = item.clientHeight;
      if (nScrollTop + nDivHight >= nScrollHight) {
        console.log("fff");
        $(".md-btn").fadeOut();
      } else {
        console.log("tttt");
        $(".md-btn").show();
      }
    });
  });
}
showMoreBtn();

$(".toggle-btn").on("click", function () {
  $(this).parents(".toggle-box").find(".toggle-content").slideToggle();
  $(this).parents(".toggle-box").toggleClass("active");
});

// =================== select控制 =====================
$(".select-box").on("click", function (e) {
  var options = $(this).parents(".selection-box").find(".select-options");
  e.stopPropagation();
  if ($(this).parents(".selection-box").hasClass("disabled")) {
  } else {
    $(this).toggleClass("on");
    options.slideToggle();
  }
});
$(".select-options li").on("click", function (e) {
  $(this)
    .parents(".selection-box")
    .find(".select-box")
    .addClass("active-color");
  $(this).addClass("selected").siblings().removeClass("selected");
  $(this).parents(".selection-box").find(".select-box").html($(this).html());
  $(this).parents(".select-options").slideUp();
  $(this).parents(".selection-box").find(".select-box").removeClass("on");
});
// =================== 彈窗控制 =====================
//關閉彈窗
function closeDialog() {
  $(".dialog-area").animate({ bottom: "-100%" });
  $(".dialog-cover").hide();
  $(".dialog-content").fadeOut();
}
//開啟彈窗
function openDialog(openObj) {
  $(".dialog-content").hide();
  $(".dialog-area").animate({ bottom: "0" });
  $(".dialog-cover").fadeIn();
  $(openObj).show();
}
// =================== 成功畫面控制 =====================
//開啟成功畫面
function openSuccess(openView) {
  $(openView).fadeIn();
  hideVerificationSuccess();
}
//關閉成功畫面
function hideVerificationSuccess() {
  setTimeout(function () {
    $(".transfer_success").fadeOut();
    $(".setting_success").fadeOut();
    $(".payBill_success").fadeOut();
    closeDialog();
  }, 2500);
}
